package com.aem.workflowstep;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.exec.WorkflowData;
import com.adobe.granite.workflow.exec.WorkflowProcess;
import com.adobe.granite.workflow.metadata.MetaDataMap;
import April.core.utils.AWSV4Auth;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;


//This is a component so it can provide or consume services
@Component
@Service
@Properties({
        @Property(name = Constants.SERVICE_DESCRIPTION, value = "Test Email workflow process implementation."),
        @Property(name = Constants.SERVICE_VENDOR, value = "Adobe"),
        @Property(name = "process.label", value = "CDN Cache Clearance") })
public class ClearCDNCacheStep implements WorkflowProcess {

    /** Default log. */
    protected final Logger log = LoggerFactory.getLogger(this.getClass());
    private String awsAccessKeyID = "AKIAIDYXXI64KCJNF7PA";
    private String awsSecretKey = "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";
    private String url = "https://cloudfront.amazonaws.com/2010-11-01/distribution/E25IWCB8GL1Q8D/invalidation";
    private String body = "<InvalidationBatch>\n" +
            "    <Path>/content/brands/sg/en/whats-new/events/body-sos.html</Path>\n" +
            "    <CallerReference>batch"+getTimeStamp()+"</CallerReference>\n" +
            "</InvalidationBatch>";

    private static final String UTF8_CHARSET = "UTF-8";
    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";
    private static final String REQUEST_URI = "/onca/xml";
    private static final String REQUEST_METHOD = "POST";
    private SecretKeySpec secretKeySpec = null;
    private Mac mac = null;

    public void execute(WorkItem item, WorkflowSession wfsession, MetaDataMap args) throws WorkflowException {
        HttpClient client;
        try {

            WorkflowData wfData = item.getWorkflowData();
            String pagePath = wfData.getPayload().toString();

            String body = "<InvalidationBatch>\n" +
                    "    <Path>"+pagePath+"</Path>\n" +
                    "    <CallerReference>batch"+getTimeStamp()+"</CallerReference>\n" +
                    "</InvalidationBatch>";

            TreeMap<String, String> awsHeaders = new TreeMap<String, String>();
            awsHeaders.put("host", url);

            AWSV4Auth aWSV4Auth = new AWSV4Auth.Builder(awsAccessKeyID, awsSecretKey)
                    .regionName("ap-southeast-1")
                    .serviceName("s3") // es - elastic search. use your service name
                    .httpMethodName("POST") //GET, PUT, POST, DELETE, etc...
                    .canonicalURI(pagePath) //end point
                    .queryParametes(null) //query parameters if any
                    .awsHeaders(awsHeaders) //aws header parameters
                    .payload(null) // payload if any
                    .debug() // turn on the debug mode
                    .build();

            client = new DefaultHttpClient();
            HttpPost post = new HttpPost(url);
            /* Get header calculated for request */
            Map<String, String> header = aWSV4Auth.getHeaders();
            for (Map.Entry<String, String> entrySet : header.entrySet()) {
                String key = entrySet.getKey();
                String value = entrySet.getValue();
                post.setHeader(key, value);
            }
            post.setEntity(new StringEntity(body));
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            String responseString = EntityUtils.toString(entity, "UTF-8");
            log.info(response.toString());

        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.toString());
        }finally {

        }
    }

    private String getTimeStamp() {
        DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));//server timezone
        return dateFormat.format(new Date());
    }
}
